#include <iostream>
using std::cout;
using std::cin;
using std::endl;
#include <string>
using std::string;
using std::to_string;
#include <math.h>
#include <ctime>
#include <algorithm>
#include <vector>
using std::vector;


int genGrades();
string genSSID();
string genMajor();
string genFname();
string genLname();
void genRecord(int myAmount);
bool compareGrade();
int counter();
void tableofValues();

int main(){
 /* srand(time(NULL));                  //This is for assignment 1
  int myAmount;

  cout << "How many student records do you want?" << endl;
  cin >> myAmount;

  genRecord(myAmount);
*/


tableofValues();

  return 0;
}

string genSSID(){

	int myAreacode = 0;
	int myGroupnum = 0;
	int mySerialnum = 0;
	char buffer [12];
	myAreacode = rand() % 1000;
    myGroupnum = rand() % 100;
    mySerialnum = rand() % 10000;
	if(mySerialnum < 1000){
		mySerialnum = mySerialnum*10;
	}
	
	string temp1 = to_string(myAreacode);
	string temp2 = to_string(myGroupnum);
	string temp3 = to_string(mySerialnum);
  
	string mySSID = temp1 + '-' + temp2 + '-' + temp3; 
	/*cout << "SSID: " << myAreacode << '-' << myGroupnum <<'-' << mySerialnum;                       //Ignore this, just for testing 
    */                                                                         
  return mySSID;
}

string genMajor(){

  string myMajor;

    int myRand = rand() % 100;
     if(myRand <= 50){
       myMajor = "CS";
     }
     else if(myRand > 50 && myRand <= 67){
       myMajor = "EE";
     }
     else if(myRand > 67 && myRand <= 84){
       myMajor = "CE";
     }
	 else if(myRand > 84 && myRand <= 100){
       myMajor = "ME";
    }

   /*cout << myMajor << endl;
   */

  return myMajor;
}

string genFname(){

  static const char lowerAlpha[27] = "abcdefghijklmnopqrstuvwxyz";
  static const char upperAlpha[27] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  char myRand1;
  char myRand2;
  string myFname;
  myFname.clear();

  myRand1 = upperAlpha[rand() % (26-0 + 0) + 0];

  const int nameLength = rand()%(12-1 + 1) + 1; //int randNum = rand()%(max-min + 1) + min;
  myFname = myRand1;

  for(int i = 0; i < nameLength; i++){

    myRand2 = lowerAlpha[rand() % (26-0 + 1) + 0];

    myFname = myFname + myRand2;

  }

    return myFname;
}
string genLname(){                            //same thing as genFname

  static const char lowerAlpha[27] = "abcdefghijklmnopqrstuvwxyz";
  static const char upperAlpha[27] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  char myRand1;
  char myRand2;
  string myLname;

  myRand1 = upperAlpha[rand() % (26-0 + 0) + 0];

  const int nameLength = rand()%(12-1 + 1) + 1; //int randNum = rand()%(max-min + 1) + min;
  myLname = myRand1;

  for(int i = 0; i < nameLength; i++){

    myRand2 = lowerAlpha[rand() % (26-0 + 1) + 0];

    myLname = myLname + myRand2;
  }




  return myLname;
}

float genGrade(int mean, int stddev){     //Box muller method to develop grades. Taken from
    static float n2 = 0.0;                //stackoverflow which was taken from a now dead link
    static int n2_cached = 0;             //because I couldn't understand the link given
    if (!n2_cached){
        float x, y, r;
        do{
            x = 2.0*rand()/RAND_MAX - 1;
            y = 2.0*rand()/RAND_MAX - 1;

            r = x*x + y*y;
        }
        while (r == 0.0 || r > 1.0);{
            float d = sqrt(-2.0*log(r)/r);
            float n1 = x*d;
            n2 = y*d;
            float result = n1*stddev + mean;
            n2_cached = 1;
            return result;
        }
    }
    else{
        n2_cached = 0;
        return n2*stddev + mean;
    }
}

float genMidterm1(){
  return genGrade(60,12);
}

float genMidterm2(){
  return genGrade(55,13);
}

float genFinal(){
  return genGrade(46,14);
}
/*
void genRecord(int myAmount){

  for(int i = 0; i < myAmount; ++i){
  cout << "First Name: " << genFname() << ' ' << "Last Name: " << genLname() << " | " <<"Major: " << genMajor() << " | " <<"Midterm1: " << genMidterm1() << " | " <<"Midterm2: " << genMidterm2() << " | " <<"Final: " << genFinal() << " | " <<"SSID: " << genSSID() << " | " << endl << endl;
  }
}
*/
/*
vector genRecord(){


string myFname = genFname();
string myLname = genLname();
string myMajor = genMajor();
float myMidterm1 = genMidterm1();
float myMidterm2 = genMidterm2();
float myFinal = genFinal();

vector <int> myRecord (myMidterm1, myMidterm2, myFinal);
}

char preCurve(){



}

*/

struct Student
{
	string myFname;
	string myLname;
	string myMajor;
	float myMidterm1;
	float myMidterm2;
	float myFinal;
	float postMidterm1;
	float postMidterm2;
	float postFinal;
	char precurveGrade;
	char postcurveGrade; 
	int secretCode;                           //the codes just represent ranking of grade 5-1 5 being the highest
	int anothersecretCode;
	int thirdCode;
};


Student genRecord(){

	Student myRecord;

	myRecord.myFname = genFname();
	myRecord.myLname = genLname();
	myRecord.myMajor = genMajor();
	myRecord.myMidterm1 = genMidterm1();
	myRecord.myMidterm2 = genMidterm2();
	myRecord.myFinal = genFinal();
	
	return myRecord;
}

vector <Student> createVector(){
	
	vector <Student> unsortedVector;
	Student myRecord;	
	for(int i = 0; i < 300; ++i){
		
		myRecord = genRecord();
		vector <Student> unsortedVector;
		
		unsortedVector.push_back(myRecord);
	}

	return unsortedVector;
}

vector <Student> mySort(){
		
	vector<Student> unsortedVector = createVector();

	int myValue = unsortedVector.size();

	vector <Student> sortedVector;


	for(int i = 0; i < myValue-1; ++i){
		for(int j = 0; j < myValue-i-1; ++j){
		
			if(((unsortedVector[i].myMidterm1+unsortedVector[i].myMidterm2+unsortedVector[i].myFinal)/3) > ((unsortedVector[i+1].myMidterm1+unsortedVector[i+1].myMidterm2+unsortedVector[i+1].myFinal)/3)){ 
			
				std::swap(unsortedVector[i],unsortedVector[i+1]);
			}
		}
	
	
	}

	sortedVector = unsortedVector;

	return sortedVector;
}


vector <Student> preCurve(){

	vector <Student> sortedVector = mySort();

	int i;
	int myValue = sortedVector.size();

	for(i = 0; i < myValue-1; ++i){
	
		if(i < 15){
	
		sortedVector[i].precurveGrade ='A'; 
		sortedVector[i].secretCode = 5;
	
		}
		else if(i >= 15 && i < 36){
			sortedVector[i].precurveGrade = 'B';
			sortedVector[i].secretCode = 4;

		}
		else if(i >=36 && i < 61){
			sortedVector[i].precurveGrade = 'C';
			sortedVector[i].secretCode = 3;
	
		}
		else if(i >= 61 && i < 80){
			sortedVector[i].precurveGrade = 'D';
			sortedVector[i].secretCode = 2;

		}

		else{
			sortedVector[i].precurveGrade = 'F';
			sortedVector[i].secretCode = 1;

		}

	}

	return sortedVector;
}


vector <Student> postCurve(){

	vector <Student> newVector = preCurve();
	int myValue = newVector.size();
	int i;
		for(int i = 0; i < myValue; ++i){
			if(newVector[i].myMidterm2 >= 30+ newVector[i].myMidterm1){
				
				newVector[i].myMidterm1 = newVector[i].myMidterm1 + newVector[i].myMidterm1*.2;
			
			}
			else if(newVector[i].myMidterm2 <= newVector[i].myMidterm1 - 25){
			
			newVector[i].postMidterm1 = newVector[i].myMidterm1 - newVector[i].myMidterm1*.15;
			
			}
		
		
		}

		for(int i = 0; i < myValue; ++i){
			if(newVector[i].myFinal >= 30+ newVector[i].myMidterm2){
					
				newVector[i].postMidterm2 = newVector[i].myMidterm2 + newVector[i].myMidterm2*.2;
			
			}
			else if(newVector[i].postFinal <= newVector[i].myMidterm2 - 25){
		
			newVector[i].myMidterm2 = newVector[i].myMidterm2 - newVector[i].myMidterm2*.15;
			
			}
		
		
		}
	
		for(i = 0; i < myValue-1; ++i){
	
			if(i < 15){
	
			newVector[i].precurveGrade ='A';
			newVector[i].anothersecretCode = 5;
	
			}
			else if(i >= 15 && i < 36){
				newVector[i].precurveGrade = 'B';
				newVector[i].anothersecretCode = 4;

			}
			else if(i >=36 && i < 61){
				newVector[i].precurveGrade = 'C';
				newVector[i].anothersecretCode = 3;
	
			}
			else if(i >= 61 && i < 80){
				newVector[i].precurveGrade = 'D';
				newVector[i].anothersecretCode = 2;

			}

			else{
				newVector[i].precurveGrade = 'F';
				newVector[i].anothersecretCode = 1;

			}

		}


	return newVector;
}
vector <Student>  mysecondSort(){

	vector <Student> unsortedVector = postCurve();
	int myValue = unsortedVector.size();
	vector <Student> finalVector;


	for(int i = 0; i < myValue-1; ++i){
		for(int j = 0; j < myValue-i-1; ++j){
		
			if(((unsortedVector[i].postMidterm1+unsortedVector[i].postMidterm2+unsortedVector[i].postFinal)/3) > ((unsortedVector[i+1].postMidterm1+unsortedVector[i+1].postMidterm2+unsortedVector[i+1].postFinal)/3)){ 
			
				std::swap(unsortedVector[i],unsortedVector[i+1]);
			}
		}
	
	
	}

	finalVector = unsortedVector;





	return finalVector;
}



vector <Student> comparePrepost(){

	vector <Student> myRecords = mysecondSort();	
	int myValue = myRecords.size();
	int i;

	for(int i = 0; i < myValue-1; ++i){
		if(myRecords[i].anothersecretCode > myRecords[i].secretCode){
		
			myRecords[i].thirdCode = 0;
		
		} 
		else if(myRecords[i].anothersecretCode < myRecords[i].secretCode){
		
			myRecords[i].thirdCode = 1;
		
		} 
		else
			myRecords[i].thirdCode = 2;
	}
		



	return myRecords;
}

void tableofValues(){


	vector <Student> myRecords = comparePrepost();
	int myValue = myRecords.size();
	int counter1 = 0;                   //number of students low -> high
	int counter2 = 0;                   //number of students high -> low
	int counter3 = 0;                   //number of students ==
	int counterA = 0;
	int counterB = 0;
	int counterC = 0;
	int counterD = 0;
	float percentOne = 0.0;
	float percentTwo = 0.0;
	float percentThree = 0.0;
	for(int i =0; i < myValue; ++i){
	if(myRecords[i].thirdCode == 0){
		counter1++;
	}
	else if(myRecords[i].thirdCode == 1){
		counter2++;
	}
	else if(myRecords[i].thirdCode == 1){
		counter3++;
	}
	
	}
	percentOne = counter1/300;
	percentTwo = counter2/300;
	percentThree = counter3/300;

	cout <<"Number of students promoted to higher grades by the rules: " << counter1 << endl;
	

	for(int i =0; i < myValue; ++i){
		if(myRecords[i].postcurveGrade == 'A' && myRecords[i].thirdCode == 0){
			counterA++;
		}
		else if(myRecords[i].postcurveGrade == 'B' && myRecords[i].thirdCode == 0){
			counterB++;
		}
		else if(myRecords[i].postcurveGrade == 'C' && myRecords[i].thirdCode == 0){
			counterC++;
		}
		else if(myRecords[i].postcurveGrade == 'D' && myRecords[i].thirdCode == 0){
			counterD++;
		}
	}
	

	cout <<"     - A from lower grade: " << counterA << endl;
	cout <<"     - B from lower grade: " << counterB << endl;
	cout <<"     - C from lower grade: " << counterC << endl;
	cout <<"     - D from lower grade: " << counterD << endl;
	cout <<"     - " << percentOne <<'%'<< " of enrollment" << endl;

	cout <<"Number of students descended to lower grades by the rules: " << counter2 << endl;
	

	for(int i =0; i < myValue; ++i){
	
		 if(myRecords[i].postcurveGrade == 'B' && myRecords[i].thirdCode == 1){
			counterA++;
		}
		else if(myRecords[i].postcurveGrade == 'C' && myRecords[i].thirdCode == 1){
			counterB++;
		}
		else if(myRecords[i].postcurveGrade == 'D' && myRecords[i].thirdCode == 1){
			counterC++;
		}
		else if(myRecords[i].postcurveGrade == 'F' && myRecords[i].thirdCode == 1){
			counterD++;
		}
	}
	
	cout <<"      -B from higher grade: " << counterA << endl;
	cout <<"      -C from higher grade: " << counterB << endl;
	cout <<"      -D from higher grade: " << counterC << endl;
	cout <<"      -F from higher grade: " << counterD << endl;
	cout <<"     - " << percentTwo <<'%'<< " of enrollment" << endl;
	cout <<"Number of students staying in the same grades: " << counter3 << endl;



}
